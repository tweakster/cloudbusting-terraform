output "vpc" {
  value       = aws_vpc.this
  description = "VPC Output"
}

output "public_route_table" {
  value       = aws_route_table.public.id
  description = "The public (Internet access via direct gateway) route table"
}
