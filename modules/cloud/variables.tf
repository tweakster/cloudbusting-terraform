variable "name" {
  description = "Cloud name"
  type        = string
}

variable "cidr_block" {
  description = "Primary IPv4 CIDR block"
  type        = string
}
